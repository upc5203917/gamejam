#pragma once
#include "CuadroMenu.h"
#include "helicopters.h"
#include <string>
#include <algorithm> // Necesario para find
void elicopter4(int x, int y) {
    elegirColor(2);
    gotoxy(x, y); cout << "     ________";
    gotoxy(x, y + 1); cout << " ()__/\\_";
    gotoxy(x, y + 2); cout << "  --/\\_\\";
    gotoxy(x, y + 3); cout << "     O O";
}

void elicopter5(int x, int y) {
    elegirColor(3);
    gotoxy(x, y); cout << "     _____";
    gotoxy(x, y + 1); cout << " ()__/__";
    gotoxy(x, y + 2); cout << "  --|--";
    gotoxy(x, y + 3); cout << "     O";
}

void elicopter6(int x, int y) {
    elegirColor(7);
    gotoxy(x, y); cout << "     ______";
    gotoxy(x, y + 1); cout << " ()__|__";
    gotoxy(x, y + 2); cout << "  --|--|";
    gotoxy(x, y + 3); cout << "     O O";
}

void elicopter7(int x, int y) {
    elegirColor(5);
    gotoxy(x, y); cout << "    _____";
    gotoxy(x, y + 1); cout << " ()_/\\_\\";
    gotoxy(x, y + 2); cout << "  --/\\_";
    gotoxy(x, y + 3); cout << "     O O";
}

void elicopter8(int x, int y) {
    elegirColor(6);
    gotoxy(x, y); cout << "     ____";
    gotoxy(x, y + 1); cout << " ()_/_\\";
    gotoxy(x, y + 2); cout << "  --|--|";
    gotoxy(x, y + 3); cout << "     O O";
}

//void borrarHelicoptero(int x, int y, int a) {
//    switch (a) {
//    case 1:
//        gotoxy(x, y); cout << "              ";
//        gotoxy(x, y + 1); cout << "            ";
//        gotoxy(x, y + 2); cout << "            ";
//        gotoxy(x, y + 3); cout << "             ";
//        break;
//    case 2:
//        gotoxy(x, y); cout << "            ";
//        gotoxy(x, y + 1); cout << "            ";
//        gotoxy(x, y + 2); cout << "            ";
//        gotoxy(x, y + 3); cout << "            ";
//        break;
//    case 3:
//        gotoxy(x, y); cout << "           ";
//        gotoxy(x, y + 1); cout << "              ";
//        gotoxy(x, y + 2); cout << "           ";
//        gotoxy(x, y + 3); cout << "           ";
//        break;
//    case 4:
//        gotoxy(x, y); cout << "             ";
//        gotoxy(x, y + 1); cout << "        ";
//        gotoxy(x, y + 2); cout << "        ";
//        gotoxy(x, y + 3); cout << "        ";
//        break;
//    case 5:
//        gotoxy(x, y); cout << "          ";
//        gotoxy(x, y + 1); cout << "        ";
//        gotoxy(x, y + 2); cout << "       ";
//        gotoxy(x, y + 3); cout << "      ";
//        break;
//    case 6:
//        gotoxy(x, y); cout << "           ";
//        gotoxy(x, y + 1); cout << "        ";
//        gotoxy(x, y + 2); cout << "        ";
//        gotoxy(x, y + 3); cout << "        ";
//        break;
//    case 7:
//        gotoxy(x, y); cout << "         ";
//        gotoxy(x, y + 1); cout << "        ";
//        gotoxy(x, y + 2); cout << "       ";
//        gotoxy(x, y + 3); cout << "        ";
//        break;
//    case 8:
//        gotoxy(x, y); cout << "         ";
//        gotoxy(x, y + 1); cout << "       ";
//        gotoxy(x, y + 2); cout << "        ";
//        gotoxy(x, y + 3); cout << "        ";
//        break;
//    }
//}

void drawHelicopters(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int x5, int y5, int x6, int y6, int x7, int y7, int x8, int y8) {
    elicopter1(x1, y1, 10);
    elicopter2(x2, y2, 4);
    elicopter3(x3, y3, 1);
    elicopter4(x4, y4);
    elicopter5(x5, y5);
    elicopter6(x6, y6);
    elicopter7(x7, y7);
    elicopter8(x8, y8);
}


int chooseHelicopters(int selected[]) {
    drawHelicopters(10, 25, 30, 17, 70, 25, 70, 17, 10, 17, 30, 25, 50, 25, 50, 17);
    int selectedCount = 0;
    int posicionY = 23;
    while (selectedCount < 3) {
        gotoxy(62, 23 + selectedCount);
        cout << "Seleccione el helicóptero " << selectedCount + 1 << ": ";
        char input = _getch();
        int num = input - '0';
        if (num >= 1 && num <= 8) {
            selected[selectedCount] = num;
            gotoxy(91, posicionY);
            cout << num;
            posicionY += 1;
            ++selectedCount;
        }
    }
    return 0;
}



#pragma once
#include "CuadroMenu.h"
const int WIDTH = 155;  // Ancho 
const int HEIGHT = 43; // Alto 

const char HORIZONTALLINE = char(196);
const char VERTICALLINE = char(179);

void printHorizontalLineGrande(int y) {
    int x = 1;
    gotoxy(x, y);
    for (int i = 0; i < 153; ++i) {
        cout << HORIZONTALLINE;
    }
}
void printVerticalLineGrande(int x) {
    for (int i = 1; i < 40; ++i) {
        gotoxy(x, i);
        cout << VERTICALLINE;
    }
}
void genCornersGrandes() {
    char esquinas[4] = { char(218), char(191), char(192), char(217) };
    printSmth(0, 0, esquinas[0]);
    printSmth(154, 0, esquinas[1]);
    printSmth(0, 40, esquinas[2]);
    printSmth(154, 40, esquinas[3]);
}

// Dibujar el marco
void drawFrame() {
    genCornersGrandes();
    printHorizontalLineGrande(0);
    printVerticalLineGrande(0);
    printVerticalLineGrande(154);
    printHorizontalLineGrande(40);
}
#pragma once
#include "Funciones_extra.h"
void genVerticalLineSaveZone(int a) {
    int cuadrito = 33;
    for (int i = 0; i < 6; ++i) {
        gotoxy(a, cuadrito);
        cout << char(179);
        ++cuadrito;
    }
}

void genHorizontalLineSaveZone(int a) {
    int posX = 2;
    for (int i = 0; i < 14; ++i) {
        gotoxy(posX, a);
        cout << char(196);
        ++posX;
    }
}

void genCornersSaveZone() {
    char esquinas[4] = { char(218), char(191), char(192), char(217) };
    printSmth(1, 32, esquinas[0]);
    printSmth(16, 32, esquinas[1]);
    printSmth(1, 39, esquinas[2]);
    printSmth(16, 39, esquinas[3]);
}

void genSaveZone() {
    setColor(LIGHTMAGENTA);
    genHorizontalLineSaveZone(32);
    genVerticalLineSaveZone(1);
    genVerticalLineSaveZone(16);
    printSmth(16, 35, ' ');
    printSmth(16, 36, ' ');
    genHorizontalLineSaveZone(39);
    genCornersSaveZone();
}
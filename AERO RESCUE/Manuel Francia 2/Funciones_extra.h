#pragma once
#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <string>
#include <random>
using namespace std;
void gotoxy(int x, int y) {
    COORD coord;
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}
void hideCursor() {
    HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_CURSOR_INFO cursorInfo;
    GetConsoleCursorInfo(consoleHandle, &cursorInfo);
    cursorInfo.bVisible = false; // Oculta el cursor
    SetConsoleCursorInfo(consoleHandle, &cursorInfo);
}
void printSmth(int x, int y, char txt) {
	gotoxy(x, y);
    cout << txt;
}
enum Color {
	BLACK = 0, BLUE = 1, GREEN = 2, CYAN = 3, RED = 4, MAGENTA = 5, BROWN = 6, LIGHTGRAY = 7,
	DARKGRAY = 8, LIGHTBLUE = 9, LIGHTGREEN = 10, LIGHTCYAN = 11, LIGHTRED = 12,
	LIGHTMAGENTA = 13, YELLOW = 14, WHITE = 15
};
void setColor(Color color) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, color);
}
void elegirColor(int x) {
	switch (x) {
	case 1: setColor(BLUE); break;
	case 2: setColor(GREEN); break;
	case 3: setColor(CYAN); break;
	case 4: setColor(RED); break;
	case 5: setColor(MAGENTA); break;
	case 6: setColor(BROWN); break;
	case 7: setColor(LIGHTGRAY); break;
	case 8: setColor(DARKGRAY); break;
	case 9: setColor(LIGHTBLUE); break;
	case 10: setColor(LIGHTGREEN); break;
	case 11: setColor(LIGHTCYAN); break;
	case 12: setColor(LIGHTRED); break;
	case 13: setColor(LIGHTMAGENTA); break;
	case 14: setColor(YELLOW); break;
	}
}
void time() {
	random_device rd;
	uniform_int_distribution<int> gen(60, 120);
	int num = gen(rd);
	for (int i = num; i >= 0; --i) {
		gotoxy(147, 1);
		cout << (i / 60) << ":";
		if (num % 60 > 9) { // Verifica si el segundo d�gito es mayor que 9
			cout << (i % 60);
		}
		else { // Si es menor que 10, imprime un 0 antes del segundo d�gito
			cout << "0" << (num % 60);
		}
		Sleep(100);
	}
}

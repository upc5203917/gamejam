#pragma once
#include "helicopters.h"
#include "RandomBots.h"
#include "CheckColision.h"

int main2() {
    random_device rd;
    uniform_int_distribution<int> gen(60, 120);
    int x1 = 3, y1 = 1; 
    int x2 = 3, y2 = 5; 
    int x3 = 3, y3 = 10;
    int selectHelicopter = 1;
    int salvadosH1 = 0;
    int salvadosH2 = 0;
    int salvadosH3 = 0;
    int num = gen(rd);
    int tiempo = 0;
    int enemiguitos = 0;
    int contadorenemigos = 0;
    int tiempoaparicionenemigos = 0;
    int salvados = 0;
    int personasabordo = 0; 
    int saveZonex = 2, saveZoney = 33;
    int colorH1 = 4, colorH2 = 1, colorH3 = 10;
    int enemigo2x = 35, enemigo3x = 50, enemigo4x = 65, enemigo5x = 80, enemigo6x = 95, enemigo7x = 110, enemigo8x = 124;
    int enemigo2Y = 31, enemigo3Y = 24, enemigo4Y = 15, enemigo5Y = 30, enemigo6Y = 38, enemigo7Y = 20, enemigo8Y = 6;
    int* personasYarriba = new int[10];
    int* personasYabajo = new int[10];
    for (int i = 0; i < 10; ++i) {
        genRandomBotLimits(personasYarriba, personasYabajo, i);
        
    }
    hideCursor();

    
    system("cls");
    gotoxy(WIDTH / 2 - 10, HEIGHT / 2);
    cout << "Presione una tecla para iniciar";
    _getch(); 

    system("cls");
    
    setColor(WHITE);
    drawFrame();
    drawHelicopters(x1, y1, x2, y2, x3, y3, colorH1, colorH2, colorH3);
    genSaveZone();
    int a1, a2, a3, a4;
    int b1, b2, b3, b4;
    genRandomStones(37, a1, b1);
    genRandomStones(67, a2, b2);
    genRandomStones(97, a3, b3);
    genRandomStones(125, a4, b4);
    genRandomBot(personasYarriba, contadorenemigos);
    for (;;) {
        for (int i = 0; i < 10; ++i) {
            if (personasabordo == 0) {
                checkColisionleft(x1, y1, personasYarriba, i, personasYabajo, colorH3, i + 1, personasabordo);
                checkColisionleft(x2, y2, personasYarriba, i, personasYabajo, colorH1, i + 1, personasabordo);
                checkColisionleft(x3, y3, personasYarriba, i, personasYabajo, colorH2, i + 1, personasabordo);
            }
           checkSaveZone(x1, y1, salvados, saveZonex, saveZoney, personasabordo, selectHelicopter, colorH3, salvadosH1);
           checkSaveZone(x2, y2, salvados, saveZonex, saveZoney, personasabordo, selectHelicopter, colorH1, salvadosH2);
           checkSaveZone(x3, y3, salvados, saveZonex, saveZoney, personasabordo, selectHelicopter, colorH2, salvadosH3);
        }
        ++tiempoaparicionenemigos;
        if (contadorenemigos < 9) {
            if (tiempoaparicionenemigos == 50) {
                tiempoaparicionenemigos = 0;
                elegirColor(contadorenemigos + 1);
                genRandomBot(personasYarriba, contadorenemigos);
                ++contadorenemigos;
            }
        }
        if (_kbhit()) {
            int ch = _getch();

            switch (ch) {
            case 72: 
            case 80: 
            case 75: 
            case 77: 
                moveHelicopter(ch, (selectHelicopter == 1) ? x1 : ((selectHelicopter == 2) ? x2 : x3), (selectHelicopter == 1) ? y1 : ((selectHelicopter == 2) ? y2 : y3), selectHelicopter, a1, a2, a3, a4, b1, b2, b3, b4, colorH1 ,colorH2 , colorH3);
                break;
            case 97: selectHelicopter = 1; break; 
            case 98: selectHelicopter = 2; break; 
            case 99: selectHelicopter = 3; break; 
            case 27: return 0;  
            }

          

        }
        ++tiempo;
        ++enemiguitos;
        if (tiempo == 100) {
            tiempo = 0;
            num -= 1;
        }
        setColor(BLUE);
        if (enemiguitos == 40) {
            enemiguitos = 0;
            
            gotoxy(enemigo2x, enemigo2Y); cout << " "; gotoxy(enemigo2x, enemigo2Y + 1); cout << "@"; ++enemigo2Y; if (enemigo2Y == 39) { gotoxy(enemigo2x, enemigo2Y); cout << " "; enemigo2Y = 1; gotoxy(enemigo2x, enemigo2Y + 1); cout << "@";}
            gotoxy(enemigo3x, enemigo3Y); cout << " "; gotoxy(enemigo3x, enemigo3Y + 1); cout << "@"; ++enemigo3Y; if (enemigo3Y == 39) { gotoxy(enemigo3x, enemigo3Y); cout << " "; enemigo3Y = 1; gotoxy(enemigo3x, enemigo3Y + 1); cout << "@";}
            gotoxy(enemigo4x, enemigo4Y); cout << " "; gotoxy(enemigo4x, enemigo4Y + 1); cout << "@"; ++enemigo4Y; if (enemigo4Y == 39) { gotoxy(enemigo4x, enemigo4Y); cout << " "; enemigo4Y = 1; gotoxy(enemigo4x, enemigo4Y + 1); cout << "@";}
            gotoxy(enemigo5x, enemigo5Y); cout << " "; gotoxy(enemigo5x, enemigo5Y + 1); cout << "@"; ++enemigo5Y; if (enemigo5Y == 39) { gotoxy(enemigo5x, enemigo5Y); cout << " "; enemigo5Y = 1; gotoxy(enemigo5x, enemigo5Y + 1); cout << "@";}
            gotoxy(enemigo6x, enemigo6Y); cout << " "; gotoxy(enemigo6x, enemigo6Y + 1); cout << "@"; ++enemigo6Y; if (enemigo6Y == 39) { gotoxy(enemigo6x, enemigo6Y); cout << " "; enemigo6Y = 1; gotoxy(enemigo6x, enemigo6Y + 1); cout << "@";}
            gotoxy(enemigo7x, enemigo7Y); cout << " "; gotoxy(enemigo7x, enemigo7Y + 1); cout << "@"; ++enemigo7Y; if (enemigo7Y == 39) { gotoxy(enemigo7x, enemigo7Y); cout << " "; enemigo7Y = 1; gotoxy(enemigo7x, enemigo7Y + 1); cout << "@";}
            gotoxy(enemigo8x, enemigo8Y); cout << " "; gotoxy(enemigo8x, enemigo8Y + 1); cout << "@"; ++enemigo8Y; if (enemigo8Y == 39) { gotoxy(enemigo8x, enemigo8Y); cout << " "; enemigo8Y = 1; gotoxy(enemigo8x, enemigo8Y + 1); cout << "@";}
        }
        gotoxy(141, 1); cout << "Time: " << (num / 60) << ":";
        if (num % 60 > 9) {
            cout << (num % 60);
        }
        else { 
            cout << "0" << (num % 60);
        }
        if (num == -1) {
            break;
        }
        drawHelicopters(x1, y1, x2, y2, x3, y3,colorH1,colorH2,colorH3);
    }
    system("cls");
    gotoxy(51, 13); cout << "TOTAL DE SALVADOS";
    gotoxy(40, 15);cout << "Cantidad de salvados del helicoptero 1: " << salvadosH1 << endl;
    gotoxy(40, 16);cout << "Cantidad de salvados del helicoptero 2: " << salvadosH2 << endl;
    gotoxy(40, 17);cout << "Cantidad de salvados del helicoptero 3: " << salvadosH3 << endl;
    delete[] personasYabajo;
    delete[] personasYarriba;

    return 0;
}
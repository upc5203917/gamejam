#pragma once
#include "Funciones_extra.h"
void printA() {
    gotoxy(54, 6);
    cout << char(201) << char(205) << char(187) << endl;
    gotoxy(54, 7);
    cout << char(186) << " " << char(186) << endl;
    gotoxy(54, 8);
    cout << char(204) << char(205) << char(185) << endl;
    gotoxy(54, 9);
    cout << char(186) << " " << char(186) << endl;
}
void printE(int x) {
    gotoxy(x, 7);
    cout << char(201) << char(205) << char(187);
    gotoxy(x, 8);
    cout << char(204) << char(205) << char(188) << endl;
    gotoxy(x, 9);
    cout << char(200) << char(205) << char(205);
}
void printr() {
    gotoxy(62, 7);
    cout << char(204) << char(205) << char(205);
    gotoxy(62, 8);
    cout << char(186);
    gotoxy(62, 9);
    cout << char(186);
}
void printO() {
    gotoxy(66, 7);
    cout << char(201) << char(205) << char(187);
    gotoxy(66, 8);
    cout << char(186) << " " << char(186);
    gotoxy(66, 9);
    cout << char(200) << char(205) << char(188);
}
void printR() {
    gotoxy(72, 6);
    cout << char(201) << char(205) << char(187) << endl;
    gotoxy(72, 7);
    cout << char(186) << char(205) << char(188) << endl;
    gotoxy(72, 8);
    cout << char(186) << char(200) << char(187) << endl;
    gotoxy(72, 9);
    cout << char(186) << " " << char(186) << endl;
}
void printS() {
    gotoxy(80, 7);
    cout << char(201) << char(205) << char(205);
    gotoxy(80, 8);
    cout << char(200) << char(205) << char(187);
    gotoxy(80, 9);
    cout << char(205) << char(205) << char(188);
}
void printC() {
    gotoxy(84, 7);
    cout << char(201) << char(205) << char(205);
    gotoxy(84, 8);
    cout << char(186);
    gotoxy(84, 9);
    cout << char(200) << char(205) << char(205);
}
void printU() {
    gotoxy(88, 7);
    cout << char(186) << " " << char(186);
    gotoxy(88, 8);
    cout << char(186) << " " << char(186);
    gotoxy(88, 9);
    cout << char(200) << char(205) << char(188);
}

#pragma once
#include "Funciones_extra.h"
#include <random>
void genRandomBots() {
    random_device rd;
    uniform_int_distribution<int> gen(20, 150);
    uniform_int_distribution<int> generar(4, 20);
    uniform_int_distribution<int> generador(1, 14);
    int personitasX[10];
    int personitasY[10];
    int posicionesarregloX = 0;
    int posicionesarregloY = 0;
    int color = generador(rd);

    for (int j = 0; j < 10; ++j) {
        personitasX[j] = gen(rd);
        personitasY[j] = generar(rd);
   }

    for (int i = 0; i < 10; ++i) {
        elegirColor(color);
        gotoxy(personitasX[i], personitasY[i]);
        cout << "#";
        gotoxy(personitasX[i] - 1, personitasY[i] + 1);
        cout << "###";
        gotoxy(personitasX[i], personitasY[i] + 2);
        cout << "#";
        color = generador(rd);
    }
}
void genRandomStones(int x, int &a, int &b) {
    random_device rd;
    uniform_int_distribution<int> gen(5, 34);
    int num = gen(rd);
    setColor(LIGHTGRAY);
    a = num - 2; b = num + 2;
    gotoxy(x + 2, num - 2);
    cout << "+++++";
    gotoxy(x + 1, num - 1);
    cout << "+++++++";
    gotoxy(x, num);
    cout << "+++++++++";
    gotoxy(x + 1, num + 1);
    cout << "+++++++";
    gotoxy(x + 2, num + 2);
    cout << "+++++";
}
void genRandomBotLimits(int* a1, int* b1, int c) {
    random_device rd;
    uniform_int_distribution<int> generar(4, 35);
    uniform_int_distribution<int> gen(1, 6);
    int num = gen(rd);
    int y;
    switch (num) {
        case 1: y = 4; break;
        case 2: y = 9; break;
        case 3: y = 14; break;
        case 4: y = 19; break;
        case 5: y = 23; break;
        case 6: y = 28; break;
    }
    for (int i = 0; i < 0; ++i) {
        if (a1[i] == a1[c]) {
            num += 4;
        }
    }
    a1[c] = y;
    b1[c] = y + 2;
}
void genRandomBot(int* a, int b) {
    random_device rd;
    uniform_int_distribution<int> gen(1, 2);
    int x;
    int y = a[b];
    int num = gen(rd);
    switch (num) {
    case 1: x = 141; break;
    case 2: x = 147; break;
    }
    gotoxy(x, y);
    cout << "#";
    gotoxy(x-1, y + 1);
    cout << "###";
    gotoxy(x, y + 2);
    cout << "#";
   

    
}

#pragma once
#include "printCuadraso.h"
void elicopter1(int x, int y, int color) {
    elegirColor(color);
    gotoxy(x, y); cout << "     _________";
    gotoxy(x, y + 1); cout << " ()____,-'-.";
    gotoxy(x, y + 2); cout << "���--._____)";
    gotoxy(x, y + 3); cout << "      -'- -'-";
}

void elicopter2(int x, int y, int color) {
    elegirColor(color);
    gotoxy(x, y); cout << "    ________";
    gotoxy(x, y + 1); cout << "()--/\\_/\\_\\";
    gotoxy(x + 1, y + 2); cout << "---|--|--|";
    gotoxy(x + 4, y + 3); cout << "O O O O";
}

void elicopter3(int x, int y, int color) {
    elegirColor(color);
    gotoxy(x, y); cout << "    ___|___";
    gotoxy(x, y + 1); cout << " __|_______|__";
    gotoxy(x, y + 2); cout << "|    |    |";
    gotoxy(x, y + 3); cout << "|____|____|";
}



void borrarElicoptero(int x, int y, int a) {
    if (a == 1) {
        gotoxy(x, y); cout << "              ";
        gotoxy(x, y + 1); cout << "            ";
        gotoxy(x, y + 2); cout << "            ";
        gotoxy(x, y + 3); cout << "             ";
    }
    else if (a == 2) {
        gotoxy(x, y); cout << "            ";
        gotoxy(x, y + 1); cout << "            ";
        gotoxy(x, y + 2); cout << "            ";
        gotoxy(x, y + 3); cout << "            ";
    }
    else if (a == 3) {
        gotoxy(x, y); cout << "           ";
        gotoxy(x, y + 1); cout << "              ";
        gotoxy(x, y + 2); cout << "           ";
        gotoxy(x, y + 3); cout << "           ";
    }
}

void drawHelicopters(int x1, int y1, int x2, int y2, int x3, int y3, int color1, int color2, int color3) {
    elicopter2(x2, y2, color1);
    elicopter3(x3, y3, color2);
    elicopter1(x1, y1, color3);
}
void moveHelicopter(int tecla, int& x, int& y, int numHelicopter, int a1, int a2, int a3, int a4, int b1, int b2, int b3, int b4, int colorH1, int colorH2, int colorH3) {
    random_device rd;
    uniform_int_distribution<int> gen(1, 3);
    uniform_int_distribution<int> generar(1, 2);
    int num = gen(rd);
    int num2 = generar(rd);
    switch (tecla) {
    case 72:  // arriba
        borrarElicoptero(x, y, numHelicopter);
        if (numHelicopter == 1) { if (y > 1) { y -= 1; }; if (y == 1) { if (x > 126) { y += 1; }; }; if (x > 23 && x < 46) { if (y >= b1 && y < b1 + 1) { y += 1; }; }; if (x > 53 && x < 76) { if (y >= b2 && y < b2 + 1) { y += 1; }; }; 
        if (x > 83 && x < 106) { if (y >= b3 && y < b3 + 1) { y += 1; }; }; if (x > 111 && x < 134) { if (y >= b4 && y < b4 + 1) { y += 1; }; }; elicopter1(x, y, colorH1); }
        else if (numHelicopter == 2) { if (y > 1) { y -= 1; }; if (y == 1) { if (x > 128) { y += 1; }; }; if (x > 25 && x < 46) { if (y >= b1 && y < b1 + 1) { y += 1; }; }; if (x > 54 && x < 76) { if (y >= b2 && y < b2 + 1) { y += 1; }; };
        if (x > 84 && x < 106) { if (y >= b3 && y < b3 + 1) { y += 1; }; }; if (x > 112 && x < 134) { if (y >= b4 && y < b4 + 1) { y += 1; }; }; elicopter2(x, y, colorH2); }
        else if (numHelicopter == 3) { if (y > 1) { y -= 1; }; if (y == 1) { if (x > 126) { y += 1; }; }; if (x > 23 && x < 46) { if (y >= b1 && y < b1 + 1) { y += 1; }; }; if (x > 53 && x < 76) { if (y >= b2 && y < b2 + 1) { y += 1; }; };
        if (x > 83 && x < 106) { if (y >= b3 && y < b3 + 1) { y += 1; }; }; if (x > 111 && x < 134) { if (y >= b4 && y < b4 + 1) { y += 1; }; }; elicopter3(x, y, colorH3); };

        break;
    case 80:  // abajo
        borrarElicoptero(x, y, numHelicopter);
        if (numHelicopter == 1) {
            if (y < 36) { y += 1; }; if (y == 29) { if (x < 17) { y -= 1; } }; if (x > 23 && x < 46) { if (y < a1 && y > a1 - 4) { y -= 1; } }; if (x > 53 && x < 76) { if (y < a2 && y > a2 - 4) { y -= 1; } };
            if (x > 83 && x < 106) { if (y < a3 && y > a3 - 4) { y -= 1; } }; if (x > 111 && x < 134) { if (y < a4 && y > a4 - 4) { y -= 1; } }; elicopter1(x, y, colorH1);
        }
        else if (numHelicopter == 2) {
            if (y < 36) { y += 1; }; if (y == 29) { if (x < 17) { y -= 1; } }; if (x > 25 && x < 46) { if (y < a1 && y > a1 - 4) { y -= 1; } }; if (x > 54 && x < 76) { if (y < a2 && y > a2 - 4) { y -= 1; } };
            if (x > 84 && x < 106) { if (y < a3 && y > a3 - 4) { y -= 1; } }; if (x > 112 && x < 134) { if (y < a4 && y > a4 - 4) { y -= 1; } }; elicopter2(x, y, colorH2);
        }
        else if (numHelicopter == 3) {
            if (y < 36) { y += 1; }; if (y == 29) { if (x < 17) { y -= 1; } }; if (x > 23 && x < 46) { if (y < a1 && y > a1 - 4) { y -= 1; } }; if (x > 53 && x < 76) { if (y < a2 && y > a2 - 4) { y -= 1; } };
            if (x > 83 && x < 106) { if (y < a3 && y > a3 - 4) { y -= 1; } }; if (x > 111 && x < 134) { if (y < a4 && y > a4 - 4) { y -= 1; } }; elicopter3(x, y, colorH3);
        }
        break;
    case 75:  // izquierda
        borrarElicoptero(x, y, numHelicopter);
        if (numHelicopter == 1) { if (x > 1) { x -= 1; }; if (x == 16 && y > 28) { x += 1; }; if (y > a1 - 4 && y < b1 + 1) { if (x == 45) { x += 1; } };
        if (y > a2 - 4 && y < b2 + 1) { if (x == 75) { x += 1; } };
        if (y > a3 - 4 && y < b3 + 1) { if (x == 105) { x += 1; } };
        if (y > a4 - 4 && y < b4 + 1) { if (x == 133) { x += 1; } };
        elicopter1(x, y, colorH1); }
        else if (numHelicopter == 2) { if (x > 1) { x -= num2; }; if (x == 16 && y > 28) { x += num2; }; if (y > a1 - 4 && y < b1 + 1) { if (x == 45) { x += num2; } };
        if (y > a2 - 4 && y < b2 + 1) { if (x == 75) { x += num2; } };
        if (y > a3 - 4 && y < b3 + 1) { if (x == 105) { x += num2; } };
        if (y > a4 - 4 && y < b4 + 1) { if (x == 133) { x += num2; } }; elicopter2(x, y, colorH2); }
        else if (numHelicopter == 3) { if (x > 3) { x -= num; }; if (x == 16 && y > 28) { x += num; }  if (y > a1 - 4 && y < b1 + 1) { if (x == 45) { x += num; } };
        if (y > a2 - 4 && y < b2 + 1) { if (x == 75) { x += num; } };
        if (y > a3 - 4 && y < b3 + 1) { if (x == 105) { x += num; } };
        if (y > a4 - 4 && y < b4 + 1) { if (x == 133) { x += num; } }; elicopter3(x, y, colorH3); num = gen(rd);
        };
        // else if (numHelicopter == 3) { if (y < 36) { y += 1; } if (y == 29) { if (x > 124) { y -= 1; } elicopter3(x, y); }; }
        break;
    case 77:
        // derecha
        borrarElicoptero(x, y, numHelicopter);
        if (numHelicopter == 1) {
            if (x < 140) { x += 1; };
            if (y == 1 && x == 126) { x -= 1; }; if (y > a1 - 4 && y < b1 + 1) { if (x == 24) { x -= 1; } };
            if (y > a2 - 4 && y < b2 + 1) { if (x == 54) { x -= 1; } };
            if (y > a3 - 4 && y < b3 + 1) { if (x == 84) { x -= 1; } };
            if (y > a4 - 4 && y < b4 + 1) { if (x == 112) { x -= 1; } };


            elicopter1(x, y, colorH1);
        }

        else if (numHelicopter == 2) {
            if (x < 140) { x += num2; }
            if (y == 1 && x == 129) { x -= num2; }; if (y > a1 - 4 && y < b1 + 1) { if (x == 26) { x -= num2; } };
            if (y > a2 - 4 && y < b2 + 1) { if (x == 55) { x -= num2; } };
            if (y > a3 - 4 && y < b3 + 1) { if (x == 85) { x -= num2; } };
            if (y > a4 - 4 && y < b4 + 1) { if (x == 113) { x -= num2; } }; elicopter2(x, y, colorH2);
            num2 = generar(rd);
        }



        else if (numHelicopter == 3) {
            if (x < 140) { x += num; }
            if (y == 1 && x == 129) { x -= num; }; if (y > a1 - 4 && y < b1 + 1) { if (x == 24) { x -= num; } };
            if (y > a2 - 4 && y < b2 + 1) { if (x == 54) { x -= num; } };
            if (y > a3 - 4 && y < b3 + 1) { if (x == 84) { x -= num; } };
            if (y > a4 - 4 && y < b4 + 1) { if (x == 112) { x -= num; } };  elicopter3(x, y, colorH3);
            num = gen(rd);
        };
        break;
    };
    }
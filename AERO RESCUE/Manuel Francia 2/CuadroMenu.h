#pragma once
#include "Letras.h"
void printHorizontalLine(int y) {
    int x = 45;
    gotoxy(x, y);
    for (int i = 0; i < 60; ++i) {
        cout << char(196);
    }
}
void printVerticalLine(int x) {
    int y = 6;
    for (int i = 0; i < 27; ++i) {
        gotoxy(x, y);
        cout << char(179);
        ++y;
    }
}
void genCorners() {
    char esquinas[4] = { char(218), char(191), char(192), char(217) };
    printSmth(44, 5, esquinas[0]);
    printSmth(104, 5, esquinas[1]);
    printSmth(44, 33, esquinas[2]);
    printSmth(104, 33, esquinas[3]);
}



void opciones() {
    gotoxy(67, 12); cout << "Iniciar Juego";
    gotoxy(67, 14); cout << "Reglas";
    gotoxy(67, 16); cout << "Elegir Veh" << char(161) << "culo";
    gotoxy(67, 18); cout << "Cr" << char(130) << "ditos";
    gotoxy(67, 20); cout << "Salir";
}
void puntero(int x, int y) {
    gotoxy(x, y); cout << ">";
}
void printRectangle() {
    printHorizontalLine(5);
    printVerticalLine(44);
    printVerticalLine(104);
    printHorizontalLine(33);
    genCorners();
}
void printMenu() {
    printA();
    printE(58);
    printr();
    printO();
    printR();
    printE(76);
    printS();
    printC();
    printU();
    printE(92);
    opciones();
    printRectangle();
}
void Creditos() {
    gotoxy(48, 6);
    cout << "Agradecemos que pruebe este juego elaborado con mucho";
    gotoxy(48, 7); cout << "esmero para usted";
    gotoxy(48, 9); cout << "Creadores: ";
    gotoxy(60, 10); cout << "Chacara Gil, Luis Joaquin";
    gotoxy(60, 11); cout << "Francia Torres, Jhony Manuel";
    gotoxy(52, 17); cout << " -.                 `|.";
    gotoxy(52, 18); cout << " |:\-,               .| \.";
    gotoxy(52, 19); cout << " |: `.------------------------------------.";
    gotoxy(52,20); cout << " / /   o o o o o o o o o o o o o.-.o o   (_`.";
    gotoxy(52,21); cout << "/_ \_              .     .=     |'|         `)";
    gotoxy(52,22); cout << "     ``\"\"\"\"\"\"\"\"\"\"\"//    /  \"\"\"\"\" `\"\"\"------\"'";
    gotoxy(52,23); cout << "                <//   /_(+";
    gotoxy(52,24); cout << "                //  /";
    gotoxy(52,25); cout << "               // /";
    gotoxy(52,26); cout << "             ----'";
}
void reglas() {
    gotoxy(46, 6); cout << "En pantalla se mostrara  tu vehiculo de rescate";
    gotoxy(46, 7); cout << "Tendras 3 vehiculos, los cuales podras elegir";
    gotoxy(46, 9); cout << "Podras cambiar de veh�culo presionando las teclas 1, 2 y 3";
    gotoxy(46, 10); cout << "1. El juego esta ambientado en un clima lluvioso, hay";
    gotoxy(46, 11); cout << "nubes las cuales no podras atravesar con tu vehiculo";
    gotoxy(46, 12); cout << "2. Deberas rescatar a las personas que se encuentran del";
    gotoxy(46, 13); cout << "lado contrario de la pantalla";
    gotoxy(46, 14); cout << "3. Solo podr�s rescatar a 1 a la vez, las personas se";
    gotoxy(46, 15); cout << "suben a tu vehiculo cuando estas cerca";
    gotoxy(46, 16); cout << "4. Tendras que llevarlas a la zona segura para poder";
    gotoxy(46, 17); cout << "rescatar a las demas";
    gotoxy(46, 18); cout << "5. Hay un tiempo predeterminado aleatorio entre 60 y 120";
    gotoxy(46, 19); cout << "segundos para realizar el rescate";
    gotoxy(46, 20); cout << "6. Si consigues rescatar a todas las personas antes, el";
    gotoxy(46, 21); cout << "juego acabar� de lo contrario, solo se mostrar� a quienes";
    gotoxy(46, 22); cout << "salvaste y con que vehiculo al acabar el juego, puedes";
    gotoxy(46, 23); cout << "regresar al menu principal y volver a jugar";
}
#include <iostream>
#include <random>
#include <Windows.h>
using namespace std;

void gotoxy(int x, int y) {
    COORD coord;
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

void printSmth(int x, int y, char txt) {
    gotoxy(x, y);
    cout << txt;
}

void genVerticalLineSaveZone(int a) {
    int cuadrito = 31;
    for (int i = 0; i < 6; ++i) {
        gotoxy(a, cuadrito);
        cout << char(179);
        ++cuadrito;
    }
}

void genHorizontalLineSaveZone(int a) {
    int posX = 140;
    for (int i = 0; i < 14; ++i) {
        gotoxy(posX, a);
        cout << char(196);
        ++posX;
    }
}

void genCornersSaveZone() {
    char esquinas[4] = { char(218), char(191), char(192), char(217) };
    printSmth(139, 30, esquinas[0]);
    printSmth(154, 30, esquinas[1]);
    printSmth(139, 37, esquinas[2]);
    printSmth(154, 37, esquinas[3]);
}

void genSaveZone() {
    genHorizontalLineSaveZone(30);
    genVerticalLineSaveZone(139);
    genVerticalLineSaveZone(154);
    printSmth(139, 33, ' ');
    printSmth(139, 34, ' ');
    genHorizontalLineSaveZone(37);
    genCornersSaveZone();
}

void genRandomBots() {
    random_device rd;
    uniform_int_distribution<int> gen(20, 150);
    uniform_int_distribution<int> generar(4, 20);
    int personitasX[10];
    int personitasY[10];
    bool ocupado[141][38] = { false }; // Matriz para registrar las posiciones ocupadas
    int posicionesarregloX = 0;
    int posicionesarregloY = 0;

    for (int j = 0; j < 10; ++j) {
        int newX, newY;
        do {
            newX = gen(rd);
            newY = generar(rd);
        } while (ocupado[newX][newY]); // Si la posici�n est� ocupada, busca otra posici�n

        personitasX[j] = newX;
        personitasY[j] = newY;
        ocupado[newX][newY] = true; // Marca la posici�n como ocupada

        // Actualiza la matriz de posiciones ocupadas para las �reas alrededor de la nueva "personita"
        for (int k = -3; k < 4; ++k) {
            for (int m = -1; m < 4; ++m) {
                ocupado[newX + k][newY + m] = true;
            }
        }
    }

    for (int i = 0; i < 10; ++i) {
        gotoxy(personitasX[i], personitasY[i]);
        cout << "o";
        gotoxy(personitasX[i] - 1, personitasY[i] + 1);
        cout << "/" << char(179) << "\\";
        gotoxy(personitasX[i] - 1, personitasY[i] + 2);
        cout << "/ \\";
    }
}



int main() {
    system("pause");
    genRandomBots();
    genSaveZone();
    return 0;
}
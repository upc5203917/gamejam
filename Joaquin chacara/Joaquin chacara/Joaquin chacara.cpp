﻿#include <iostream>
#include <conio.h>
#include <windows.h>
#include <string>
#include <algorithm> // Necesario para find

using namespace std;

const int WIDTH = 155;  // Ancho 
const int HEIGHT = 43; // Alto 

const char HORIZONTALLINE = char(196);
const char VERTICALLINE = char(179);

// Mover cursor
void gotoxy(int x, int y) {
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD pos = { static_cast<SHORT>(x), static_cast<SHORT>(y) };
    SetConsoleCursorPosition(hConsole, pos);
}

void printSmth(int x, int y, char txt) {
    gotoxy(x, y);
    cout << txt;
}

void printHorizontalLine(int y) {
    int x = 1;
    gotoxy(x, y);
    for (int i = 0; i < 153; ++i) {
        cout << HORIZONTALLINE;
    }
}

void printVerticalLine(int x) {
    for (int i = 1; i < 40; ++i) {
        gotoxy(x, i);
        cout << VERTICALLINE;
    }
}

void genCorners() {
    char esquinas[4] = { char(218), char(191), char(192), char(217) };
    printSmth(0, 0, esquinas[0]);
    printSmth(154, 0, esquinas[1]);
    printSmth(0, 40, esquinas[2]);
    printSmth(154, 40, esquinas[3]);
}

// Dibujar el marco
void drawFrame() {
    genCorners();
    printHorizontalLine(0);
    printVerticalLine(0);
    printVerticalLine(154);
    printHorizontalLine(40);
}

void elicopter1(int x, int y) {
    gotoxy(x, y); cout << "     _________";
    gotoxy(x, y + 1); cout << " ()____,-'-.";
    gotoxy(x, y + 2); cout << "°°°--._____)";
    gotoxy(x, y + 3); cout << "      -'- -'-";
}

void elicopter2(int x, int y) {
    gotoxy(x, y); cout << "    ________";
    gotoxy(x, y + 1); cout << " ()--/\\_/\\_\\";
    gotoxy(x, y + 2); cout << "  ---|--|--|";
    gotoxy(x, y + 3); cout << "     O O O O";
}

void elicopter3(int x, int y) {
    gotoxy(x, y); cout << "    ___|___";
    gotoxy(x, y + 1); cout << " __|_______|__";
    gotoxy(x, y + 2); cout << "|    |    |";
    gotoxy(x, y + 3); cout << "|____|____|";
}

void elicopter4(int x, int y) {
    gotoxy(x, y); cout << "     ________";
    gotoxy(x, y + 1); cout << " ()__/\\_";
    gotoxy(x, y + 2); cout << "  --/\\_\\";
    gotoxy(x, y + 3); cout << "     O O";
}

void elicopter5(int x, int y) {
    gotoxy(x, y); cout << "     _____";
    gotoxy(x, y + 1); cout << " ()__/__";
    gotoxy(x, y + 2); cout << "  --|--";
    gotoxy(x, y + 3); cout << "     O";
}

void elicopter6(int x, int y) {
    gotoxy(x, y); cout << "     ______";
    gotoxy(x, y + 1); cout << " ()__|__";
    gotoxy(x, y + 2); cout << "  --|--|";
    gotoxy(x, y + 3); cout << "     O O";
}

void elicopter7(int x, int y) {
    gotoxy(x, y); cout << "    _____";
    gotoxy(x, y + 1); cout << " ()_/\\_\\";
    gotoxy(x, y + 2); cout << "  --/\\_";
    gotoxy(x, y + 3); cout << "     O O";
}

void elicopter8(int x, int y) {
    gotoxy(x, y); cout << "     ____";
    gotoxy(x, y + 1); cout << " ()_/_\\";
    gotoxy(x, y + 2); cout << "  --|--|";
    gotoxy(x, y + 3); cout << "     O O";
}

void borrarElicoptero(int x, int y, int a) {
    switch (a) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
        gotoxy(x, y); cout << "              ";
        gotoxy(x, y + 1); cout << "              ";
        gotoxy(x, y + 2); cout << "              ";
        gotoxy(x, y + 3); cout << "              ";
        break;
    }
}

void hideCursor() {
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_CURSOR_INFO cursorInfo;
    GetConsoleCursorInfo(hConsole, &cursorInfo);
    cursorInfo.bVisible = false;
    SetConsoleCursorInfo(hConsole, &cursorInfo);

    // Desactivar el parpadeo del cursor
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    GetConsoleScreenBufferInfo(hConsole, &csbi);
    csbi.dwSize = { csbi.dwSize.X, csbi.dwSize.Y };
    SetConsoleScreenBufferSize(hConsole, csbi.dwSize);
}

void drawHelicopters(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int x5, int y5, int x6, int y6, int x7, int y7, int x8, int y8) {
    elicopter1(x1, y1);
    elicopter2(x2, y2);
    elicopter3(x3, y3);
    elicopter4(x4, y4);
    elicopter5(x5, y5);
    elicopter6(x6, y6);
    elicopter7(x7, y7);
    elicopter8(x8, y8);
}

void moveHelicopter(int tecla, int& x, int& y, int numHelicopter) {
    int newX = x, newY = y;
    switch (tecla) {
    case 72:  // arriba
        if (y > 1) newY -= 1;
        break;
    case 80:  // abajo
        if (y < 36) newY += 1;
        break;
    case 75:  // izquierda
        if (x > 3) newX -= 1;
        break;
    case 77:  // derecha
        if (x < 140) newX += 1;
        break;
    }

    // Verificar colisiones
    if ((newX != x || newY != y) && (newX >= 1 && newX <= 140 && newY >= 1 && newY <= 36)) {
        borrarElicoptero(x, y, numHelicopter);
        x = newX;
        y = newY;
    }

    // Dibujar el helicóptero correspondiente
    switch (numHelicopter) {
    case 1: elicopter1(x, y); break;
    case 2: elicopter2(x, y); break;
    case 3: elicopter3(x, y); break;
    case 4: elicopter4(x, y); break;
    case 5: elicopter5(x, y); break;
    case 6: elicopter6(x, y); break;
    case 7: elicopter7(x, y); break;
    case 8: elicopter8(x, y); break;
    }
}

void chooseHelicopters(int selected[]) {
    int selectedCount = 0;
    while (selectedCount < 3) {
        gotoxy(WIDTH / 2 - 10, HEIGHT / 2 + 2 + selectedCount);
        cout << "Seleccione el helicóptero " << selectedCount + 1 << ": ";
        char input = _getch();
        int num = input - '0';
        if (num >= 1 && num <= 8) {
            bool alreadySelected = false;
            for (int i = 0; i < 3; ++i) {
                if (selected[i] == num) {
                    alreadySelected = true;
                    break;
                }
            }
            if (!alreadySelected) {
                selected[selectedCount] = num;
                cout << num;
                selectedCount++;
            }
        }
    }
}

int main() {
    system("color 97");

    // Arreglos estáticos para almacenar las coordenadas de los helicópteros
    int selectedHelicopters[3] = { 0 }; // Inicialmente todos los helicópteros están deseleccionados
    int x[8] = { 10, 30, 50, 70, 10, 30, 50, 70 };
    int y[8] = { HEIGHT / 2 - 4, HEIGHT / 2 - 4, HEIGHT / 2 - 4, HEIGHT / 2 - 4, HEIGHT / 2 + 4, HEIGHT / 2 + 4, HEIGHT / 2 + 4, HEIGHT / 2 + 4 };
    int pos[3] = { 1, 10, 19 };

    hideCursor();

    system("cls");
    gotoxy(WIDTH / 2 - 10, HEIGHT / 2);
    cout << "Presione una tecla para iniciar";
    _getch();

    system("cls");
    drawFrame();
    gotoxy(WIDTH / 2 - 10, HEIGHT / 2 - 12);
    cout << "Escoge 3 helicópteros";

    drawHelicopters(x[0], y[0], x[1], y[1], x[2], y[2], x[3], y[3], x[4], y[4], x[5], y[5], x[6], y[6], x[7], y[7]);

    // Ahora la función de selección de helicópteros necesita operar directamente en el arreglo selectedHelicopters
    chooseHelicopters(selectedHelicopters);

    system("cls");
    drawFrame();

    int selectedIndex = 0;
    int x_selected[3] = { 1, 1, 1 };
    int y_selected[3] = { 1, 10, 19 };

    // Mostrar solo el primer helicóptero seleccionado inicialmente
    moveHelicopter(0, x_selected[0], y_selected[0], selectedHelicopters[0]);

    for (;;) {
        if (_kbhit()) {
            int ch = _getch();

            if (ch >= '1' && ch <= '3') {
                int newSelectedIndex = ch - '1';
                if (newSelectedIndex != selectedIndex) {
                    // Borrar el helicóptero anterior
                    borrarElicoptero(x_selected[selectedIndex], y_selected[selectedIndex], selectedHelicopters[selectedIndex]);
                    selectedIndex = newSelectedIndex;
                    // Dibujar el nuevo helicóptero seleccionado
                    moveHelicopter(0, x_selected[selectedIndex], y_selected[selectedIndex], selectedHelicopters[selectedIndex]);
                }
            }
            if (ch == 27) { break; }
            else {
                moveHelicopter(ch, x_selected[selectedIndex], y_selected[selectedIndex], selectedHelicopters[selectedIndex]);
            }
        }
    }

    return 0;
}

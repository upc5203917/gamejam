#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <string>
#include <random>
using namespace std;
void gotoxy(int x, int y) {
	COORD coord;
	coord.X = x;
	coord.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}
void hideCursor() {
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO cursorInfo;
	GetConsoleCursorInfo(consoleHandle, &cursorInfo);
	cursorInfo.bVisible = false; // Oculta el cursor
	SetConsoleCursorInfo(consoleHandle, &cursorInfo);
}
void printSmth(int x, int y, char txt) {
	gotoxy(x, y);
	cout << txt;
}
void time() {
	random_device rd;
	uniform_int_distribution<int> gen(60, 120);
	int num = gen(rd);
	for (int i = num; i >= 0; --i) {
		gotoxy(0, 0);
		cout << (i / 60) << ":";
		if (i % 60 > 9) { // Verifica si el segundo d�gito es mayor que 9
			cout << (i % 60);
		}
		else { // Si es menor que 10, imprime un 0 antes del segundo d�gito
			cout << "0" << (i % 60);
		}
		Sleep(100);
	}
}

void printEnemies() {
	int i = 0;
	bool movingDown = true; // Variable para controlar la direcci�n del movimiento

	for (;;) {
		gotoxy(0, i);
		cout << " ";
		gotoxy(0, movingDown ? i + 1 : i - 1);
		cout << "@";
		gotoxy(3, i);
		cout << " ";
		gotoxy(3, movingDown ? i + 1 : i - 1);
		cout << "@";
		gotoxy(6, i);
		cout << " ";
		gotoxy(6, movingDown ? i + 1 : i - 1);
		cout << "@";
		gotoxy(9, i);
		cout << " ";
		gotoxy(9, movingDown ? i + 1 : i - 1);
		cout << "@";
		gotoxy(12, i);
		cout << " ";
		gotoxy(12, movingDown ? i + 1 : i - 1);
		cout << "@";
		if (movingDown) {
			++i;
		}
		else {
			--i;
		}

		Sleep(100);

		// Cambiar la direcci�n del movimiento cuando llegue al borde
		if (i == 0 || i == 39) {
			movingDown = !movingDown;
		}

		if (_kbhit()) {
			return;
		}
	}
}


int main() {
	printEnemies();
	return 0;
}

